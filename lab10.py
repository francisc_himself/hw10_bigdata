#!/usr/bin/python
import numpy as np
from pprint import pprint
import matplotlib.pyplot as plt
from PIL import Image, ImageDraw

NB_NODES   = 10
MAX_WEIGHT =  5

TransitionMatrix = [[0 for i in range(NB_NODES)] for j in range(NB_NODES)]

graph = { 0 : [3, 4, 6, 8],
		  1 : [3, 7],
		  2 : [1, 7, 9],
		  3 : [2, 4, 6],
		  4 : [0, 2, 4, 5, 8],
		  5 : [1, 5],
		  6 : [5, 3, 7],
		  7 : [1, 3, 4, 7, 8, 9],
		  8 : [1, 3],
		  9 : [2, 4, 9]
	}

# Transition Matrix gets stored as inverted index
for idx_i in range(NB_NODES):
	# If there are outgoing nodes:
	if (len(graph[idx_i]) != 0):
		for a_target in graph[idx_i]:
			TransitionMatrix[a_target][idx_i] = int(float(1) / len(graph[idx_i]) * 1000)/ 1000.0


#####################################################
def matMult(m1, m2):
	r = []
	m = []
	for i in range(len(m1)):
		for j in range(len(m2[0])):
			sums = 0;
			for k in range(len(m2)):
				sums += m1[i][k]*m2[k][j]
			r.append(sums)
		m.append(r)
		r = []
	return m
#####################################################

def PageRank(TransitionMatrix):
	pprint(TransitionMatrix)
	randomPage = []
	# Get a random page:
	for idx_i in range(NB_NODES):
		randomPage.append([int(1/float(NB_NODES)*1000)/1000.0])
	print randomPage


	bookkepingRes = []
	for i in range(10):
		resultMul = matMult(TransitionMatrix, randomPage)
		for an_elem in resultMul:
			an_elem[0] = int(an_elem[0]*1000)/1000.0
		pprint(resultMul)
		if bookkepingRes != []:
			max_diff = 0
			print "STEP#"+str(i)+"\n### difference is:"
			for i in range(NB_NODES):
				print "diff=", resultMul[i][0] - bookkepingRes[i][0]
				if abs(resultMul[i][0] - bookkepingRes[i][0]) > max_diff:
					max_diff = abs(resultMul[i][0] - bookkepingRes[i][0])
			if abs(max_diff) < 0.002:
				break

		randomPage = resultMul
		bookkepingRes = resultMul


		# Plotting
		objects = [str(i) for i in range(NB_NODES)]
		y_pos = np.arange(len(objects))
		data = [an_elem[0] for an_elem in randomPage]
		plt.bar(y_pos, data, align='center', alpha=0.5)
		plt.xticks(y_pos, objects)
		plt.xlabel('node')
		plt.title('STEP#'+str(i))
		plt.show()


	pass



### Main ###
PageRank(TransitionMatrix)